Groupe: Mike SIP et Remy VIANNE
2A11

Avec ce nettoyage:

nettoyage<-function(corpus){
    corpus<-tm_map(corpus,merge)
    corpus<-tm_map(corpus,content_transformer(tolower))
    corpus<-tm_map(corpus,removeScript)
    corpus<-tm_map(corpus,removeBalises)
    corpus<-tm_map(corpus,removeWords, words=stopwords('en'))
    corpus<-tm_map(corpus,removePunctuation)
    corpus<-tm_map(corpus,stemDocument, language='en')
    corpus<-tm_map(corpus,removeNumbers)
}

Approche kPPV:
k le plus optimale = 3 (erreurkPPV = 0.2390476)

Pondération binaire:
k le plus optimale = 4 (erreurkPPV = 0.2571429)

Pondération tFIdf:
k le plus optimale = 1 (erreurkPPV = 0.2857143)

Ajout d'un nouveau nettoyage: (Réduire le nombre de colonnes)

nettoyage<-function(corpus){
    motsSuppr<-c(stopwords('en'),"arial","asset","background","borderpx","commentsblock","fontem","fontfamili","fontsiz","fontsize","fontweight","gtnbspnbsp","letterspacingem","lineheightem","marginem","mortgag","mstrebuchetarialverdanasansserif","nbsp","nbspnbsp","nbspnbspnbsp","nbspnbspnbspnbsp","nbspnbspnbspnbspnbsp","noaa","pad","padding","paddingpx"
    ,"textdecor","textdecorationnon","texttransformuppercas","widthpx")
    corpus<-tm_map(corpus,merge)
    corpus<-tm_map(corpus,content_transformer(tolower))
    corpus<-tm_map(corpus,removeScript)
    corpus<-tm_map(corpus,removeBalises)
    corpus<-tm_map(corpus,removeWords, words=motsSuppr)
    corpus<-tm_map(corpus,removePunctuation)
    corpus<-tm_map(corpus,stemDocument, language='en')
    corpus<-tm_map(corpus,removeNumbers)
    corpus<-tm_map(corpus,removeWords, words=motsSuppr)
}

Approche kPPV:
k le plus optimale = 4 (erreurkPPV = 0.2028571) 

Pondération binaire:
k le plus optimale = 3 (erreurkPPV = 0.2619048)

Pondération tFIdf:
k le plus optimale = 1 (erreurkPPV = 0.2847619)

Tentative d'implémentation du classifieur de Bayes -> Pas réussi

Tentative d'implémentation SVM: //Solution actuelle

corpus<-VCorpus(DirSource("training",recursive=TRUE))
corpusN<-nettoyage(corpus)
mat<-DocumentTermMatrix(corpusN)
vocab<-findFreqTerms(mat,200)
mat200<-DocumentTermMatrix(corpusN,list(dictionary=vocab))
data<-svm(x=as.matrix(mat200), y=c(rep(1,150),rep(2,150),rep(3,150),rep(4,150),rep(5,150),rep(6,150),rep(7,150)), type="C", kernel="linear")
saveRDS(vocab, file="vocab.Rdata")
saveRDS(data, file="data.Rdata")

