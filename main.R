library(tm)
library(e1071)

# -------------------- ZIP A RENDRE POUR TENTATIVE --------------------
# tentative.zip(Main.R,Readme.txt,vocab.csv,data.csv)

#un texte peut parfois etre considere comme un vecteur ou une liste de lignes.
#La fonction suivante concatene toutes les lignes du texte en parametre et retourne
#une unique chaine de caracteres
merge<-function(t){
    PlainTextDocument(paste(t,collapse=""))
}

#Suppression des scripts (<script .... </script>)
removeScript<-function(t){
    #découpage de la chaine en utilisant "<split"
    sp<-strsplit(as.character(t), "<script")[[1]]
    #pour chaque partie du split, le début (jusqu'a </script>) est supprimé
    vec<-sapply(1:length(sp),function(i) gsub(sp[i], pattern=".*</script>", replace=" "))
    #les élements du split nettoyés sont concaténés
    PlainTextDocument(paste(vec,collapse=""))
}

#Suppression de toutes les balises
removeBalises<-function(t){
    t1<-gsub("<[^>]*>", " ", t)
    #suppression des occurrences multiples d'espaces (ou de tabulations)
    PlainTextDocument(gsub("[ \t]+"," ",t1))
}

#Fonction de nettoyage permettant de récupérer uniquement le contenu textuel d'une page HTML
nettoyage<-function(corpus){
    motsSuppr<-c(stopwords('en'),"arial","asset","background","borderpx","commentsblock","fontem","fontfamili","fontsiz","fontsize","fontweight","gtnbspnbsp","letterspacingem","lineheightem","marginem","mortgag","mstrebuchetarialverdanasansserif","nbsp","nbspnbsp","nbspnbspnbsp","nbspnbspnbspnbsp","nbspnbspnbspnbspnbsp","noaa","pad","padding","paddingpx"
    ,"textdecor","textdecorationnon","texttransformuppercas","widthpx")
    corpus<-tm_map(corpus,merge)
    corpus<-tm_map(corpus,content_transformer(tolower))
    corpus<-tm_map(corpus,removeScript)
    corpus<-tm_map(corpus,removeBalises)
    corpus<-tm_map(corpus,removeWords, words=motsSuppr)
    corpus<-tm_map(corpus,removePunctuation)
    corpus<-tm_map(corpus,stemDocument, language='en')
    corpus<-tm_map(corpus,removeNumbers)
    corpus<-tm_map(corpus,removeWords, words=motsSuppr)
}

# corpus<-VCorpus(DirSource("training",recursive=TRUE))
# corpusN<-nettoyage(corpus)
# mat<-DocumentTermMatrix(corpusN)
# vocab<-findFreqTerms(mat,200)
# mat200<-DocumentTermMatrix(corpusN,list(dictionary=vocab))
# classes<-c(rep(1,150),rep(2,150),rep(3,150),rep(4,150),rep(5,150),rep(6,150),rep(7,150))
# M<-as.matrix(mat200)
# M<-cbind(M,classes)
# # mat200bin<-weightBin(mat200)
# # M<-cbind(as.matrix(mat200bin),c(rep(1,150),rep(2,150),rep(3,150),rep(4,150),rep(5,150),rep(6,150),rep(7,150)))
# # mat200tfidf<-weightTfIdf(mat200)
# # M<-cbind(as.matrix(mat200tfidf),c(rep(1,150),rep(2,150),rep(3,150),rep(4,150),rep(5,150),rep(6,150),rep(7,150)))
# saveRDS(vocab,file="vocab.Rdata")
# saveRDS(M,file="data.Rdata")

# distance<-function(x,y){
#     return(sqrt(sum((x-y)^2)))
# }

# dist.voisins<-function(vecteur,data){
#     return(apply(data,1,distance,x=vecteur))
# }

# kppv<-function(vecteur,k,data){
#     v<-dist.voisins(vecteur,data)
#     return(order(v)[1:k])
# }

# classerKPPV<-function(vecteur,k,data){
#     x<-kppv(vecteur,k,data[,-ncol(data)])
#     c<-table(data[x,ncol(data)])
#     ind<-names(c)[which.max(c)]
#     return(ind)
# }

# classer<-function(fic){
#     vocab<-readRDS(file="vocab.Rdata")
#     data<-readRDS(file="data.Rdata")
#     classerCorpus<-VCorpus(URISource(fic))
#     classerCorpusN<-nettoyage(classerCorpus)
#     classerVecteur<-DocumentTermMatrix(classerCorpusN,list(dictionary=vocab))
#     classerM<-as.matrix(classerVecteur)
#     return(c("accueil","blog","commerce","FAQ","home","liste","recherche")[strtoi(classerKPPV(classerM, 4, data))])
# }

# erreurkPPV<-function(k,data){
#     res<-sapply(1:nrow(data),function(i) return(classerKPPV(data[i,-ncol(data)],k,data[-i,])))
#     sum(res!=data[,ncol(data)])/nrow(data)
# }

# corpus<-VCorpus(DirSource("training",recursive=TRUE))
# corpusN<-nettoyage(corpus)
# mat<-DocumentTermMatrix(corpusN)
# vocab<-findFreqTerms(mat,200)
# mat200<-DocumentTermMatrix(corpusN,list(dictionary=vocab))
# data<-svm(x=as.matrix(mat200), y=c(rep(1,150),rep(2,150),rep(3,150),rep(4,150),rep(5,150),rep(6,150),rep(7,150)), type="C", kernel="linear")
# saveRDS(vocab, file="vocab.Rdata")
# saveRDS(data, file="data.Rdata")

classer<-function(fic){
    vocab<-readRDS(file="vocab.Rdata")
    data<-readRDS(file="data.Rdata")
    classerCorpus<-VCorpus(URISource(fic))
    classerCorpusN<-nettoyage(classerCorpus)
    classerVecteur<-DocumentTermMatrix(classerCorpusN,list(dictionary=vocab))
    classerM<-as.matrix(classerVecteur)
    classeIndex <- predict(data, classerM)
    return(c("accueil","blog","commerce","FAQ","home","liste","recherche")[strtoi(as.vector(classeIndex))])
}